package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class editproduct {

	@Given("the user click the product that want to be edit")
	public void click_product () {
		WebUI.scrollToElement(findTestObject('Produk/Page_SecondHand/btn_pilih_barang'), 0)
		WebUI.click(findTestObject('Produk/Page_SecondHand/btn_pilih_barang'))
	}

	@When("the user click edit button")
	public void click_edit () {
		WebUI.click(findTestObject('Produk/Page_SecondHand/a_Edit'))
	}

	@And("the user edit product detail")
	public void user_edit_product_detail() {
		WebUI.setText(findTestObject('Produk/Page_SecondHand/text_nama_produk'), ' update baju bola keren banget')
		WebUI.setText(findTestObject('Produk/Page_SecondHand/text _harga_produk'), '400000')
		WebUI.selectOptionByValue(findTestObject('Produk/Page_SecondHand/radio_kategori'), '1', false)
		WebUI.scrollToElement(findTestObject('Produk/Page_SecondHand/label_Terbitkan'), 3)
		WebUI.setText(findTestObject('Produk/Page_SecondHand/text_Deskripsi'), 'ini baju bekas ronaldo terupdate')
		///WebUI.uploadFile(findTestObject('Produk/Page_SecondHand/btn_tambah_gambar'), '')
	}

	@Then("the user click on terbitkan button")
	public void click_edit_terbitkan () {
		WebUI.click(findTestObject('Produk/Page_SecondHand/label_Terbitkan'))
	}

}
