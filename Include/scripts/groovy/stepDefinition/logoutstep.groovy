package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class logoutstep {
	@Given("User navigates to home page")

	def navigateToLogoutPage() {
		WebUI.click(findTestObject('login/Page_SecondHand/01. button_Masuk'))
	}


	@When("user enter username and password and login button")
	public void user_enter_username_and_password_loginbutton() {
		WebUI.setText(findTestObject('login/Page_SecondHand/02. text_username'), 'leokel3@binar.com')
		WebUI.setText(findTestObject('login/Page_SecondHand/03. text_password'), '123456')
		WebUI.click(findTestObject('login/Page_SecondHand/04. button_login'))
	}



	@And("Click on logout button")
	def clicklogout( ) {
		WebUI.click(findTestObject('logout/Page_SecondHand/02. button_Keluar'))
	}


	@Then("user is navigated on home")
	public void user_is_navigated_on_home() {
		WebUI.verifyElementVisible(findTestObject('logout/Page_SecondHand/03. button_masuk'))
	}
}