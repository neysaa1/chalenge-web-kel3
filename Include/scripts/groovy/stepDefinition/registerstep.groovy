package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class registerstep {
	@Given("User navigates to register page")

	def navigateRegisterPage() {
		WebUI.click(findTestObject('Registrasi/Page_SecondHand/01. button_Masuk'))
		WebUI.click(findTestObject('Registrasi/Page_SecondHand/02. button_daftar'))
	}

	@When("user enter name username and password")
	public void user_enter_name_username_and_password() {
		WebUI.setText(findTestObject('Registrasi/Page_SecondHand/03. text_Name_username'), 'Leo')

		int RN

		RN = ((Math.random() * 50000) as int)
		WebUI.setText(findTestObject('Registrasi/Page_SecondHand/04. text_Email_useremail'), ('leo' + RN) + '@binar.com')
		WebUI.setEncryptedText(findTestObject('login/Page_SecondHand/03. text_password'), 'aeHFOx8jV/A=')
	}

	@And("Click on register button")
	def clickRegister( ) {
		WebUI.click(findTestObject('Registrasi/Page_SecondHand/06. button_daftar_commit'))
	}

	@Then("user is navigated on Register homepage")
	public void user_is_navigated_on_register_homepage() {
		WebUI.verifyElementVisible(findTestObject('Registrasi/Page_SecondHand/07. verify logo'))
	}
}
