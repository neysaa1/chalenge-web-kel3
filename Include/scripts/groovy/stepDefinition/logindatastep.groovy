package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class logindatastep {
	@Given("User navigates to login data page")

	def navigateToLoginPage() {
		WebUI.click(findTestObject('login/Page_SecondHand/01. button_Masuk'))
	}


	@When("user enter data username and password")
	public void user_enter_data_username_and_password() {

		String randomString = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10)
		def email_code = ('abc' + randomString) + '@binar.com'

		def testData = [
			[('Email') : '///////////', ('Password') : '123456'],
			[('Email') : email_code, ('Password') : '123456'],
			[('Email') : email_code, ('Password') : ''],
			[('Email') : 'leokel03@binar.com', ('Password') : '123456']
		]

		for (def data : testData) {
			def email = data['Email']
			def password = data['Password']


			WebUI.setText(findTestObject('login/Page_SecondHand/02. text_username'), email)
			WebUI.setText(findTestObject('login/Page_SecondHand/03. text_password'), password)
			WebUI.click(findTestObject('login/Page_SecondHand/04. button_login'))
		}
	}


	@Then("user is navigated data on homepage")
	public void user_is_navigated_on_homepage() {
		WebUI.verifyElementVisible(findTestObject('login/Page_SecondHand/05. verify logo'))
	}
}