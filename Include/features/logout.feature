Feature: Logout Feature

Scenario: User want to logout after login

	Given User navigates to home page
	When user enter username and password and login button
	And Click on logout button
	Then user is navigated on homepage
