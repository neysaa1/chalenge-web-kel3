$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/semuaproduk.feature");
formatter.feature({
  "name": "semua produk",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "the user want to open my product list page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user is navigated to homepage to see all my product",
  "keyword": "Given "
});
formatter.match({
  "location": "semuaproduk.user_is_navigated_to_homepage_to_see_all_my_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click my product list icon button",
  "keyword": "When "
});
formatter.match({
  "location": "semuaproduk.click_my_product_list_icon_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to my product list page to see all my product",
  "keyword": "Then "
});
formatter.match({
  "location": "semuaproduk.user_is_navigated_to_product_list_to_see_all_my_product()"
});
formatter.result({
  "status": "passed"
});
});