$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/addproduct.feature");
formatter.feature({
  "name": "Add Product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "the user want to add product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user is navigated to homepage first",
  "keyword": "Given "
});
formatter.match({
  "location": "addproduct.user_is_navigated_to_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click on jual button",
  "keyword": "When "
});
formatter.match({
  "location": "addproduct.click_jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated on add product page",
  "keyword": "And "
});
formatter.match({
  "location": "addproduct.user_is_navigated_on_add_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user input product detail",
  "keyword": "And "
});
formatter.match({
  "location": "addproduct.user_enter_product_detail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click on add terbitkan button",
  "keyword": "Then "
});
formatter.match({
  "location": "addproduct.click_terbitkan()"
});
formatter.result({
  "status": "passed"
});
});