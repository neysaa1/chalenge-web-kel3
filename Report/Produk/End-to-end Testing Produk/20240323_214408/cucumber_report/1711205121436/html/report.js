$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/editproduct.feature");
formatter.feature({
  "name": "Edit product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "the user want to edit their product detail",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user click the product that want to be edit",
  "keyword": "Given "
});
formatter.match({
  "location": "editproduct.click_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click edit button",
  "keyword": "When "
});
formatter.match({
  "location": "editproduct.click_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user edit product detail",
  "keyword": "And "
});
formatter.match({
  "location": "editproduct.user_edit_product_detail()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click on terbitkan button",
  "keyword": "Then "
});
formatter.match({
  "location": "editproduct.click_edit_terbitkan()"
});
formatter.result({
  "status": "passed"
});
});