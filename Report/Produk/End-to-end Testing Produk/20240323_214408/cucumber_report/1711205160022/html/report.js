$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/deleteproduct.feature");
formatter.feature({
  "name": "delete product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to delete their product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user click the product that want to be delete",
  "keyword": "Given "
});
formatter.match({
  "location": "deleteproduct.click_product_delete()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click delete button",
  "keyword": "When "
});
formatter.match({
  "location": "deleteproduct.click_delete()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the product should be removed from product list",
  "keyword": "Then "
});
formatter.match({
  "location": "deleteproduct.user_is_navigated_to_daftar_jual_saya_page()"
});
formatter.result({
  "status": "passed"
});
});