$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/login.feature");
formatter.feature({
  "name": "Login Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to login using correct credential",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User press login button",
  "keyword": "Given "
});
formatter.match({
  "location": "login.userwenttologinpagetologin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input username and password",
  "keyword": "When "
});
formatter.match({
  "location": "login.userenterusernameandpassword()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user press login button",
  "keyword": "And "
});
formatter.match({
  "location": "login.userclickonloginbutton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is navigated on homepage to verify login",
  "keyword": "Then "
});
formatter.match({
  "location": "login.useriswenttohomepagetoverify()"
});
formatter.result({
  "status": "passed"
});
});