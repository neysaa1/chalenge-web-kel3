$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/logindata.feature");
formatter.feature({
  "name": "Logindata Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to login using data",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to login data page",
  "keyword": "Given "
});
formatter.match({
  "location": "logindatastep.navigateToLoginPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enter data username and password",
  "keyword": "When "
});
formatter.match({
  "location": "logindatastep.user_enter_data_username_and_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is navigated data on homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "logindatastep.user_is_navigated_on_homepage()"
});
formatter.result({
  "status": "passed"
});
});