$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/produkdiminati.feature");
formatter.feature({
  "name": "produk diminati",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "user ingin melihat produk yang ia minati pada daftar saya page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user click my product list menu",
  "keyword": "Given "
});
formatter.match({
  "location": "produkdiminati.click_my_product_list_menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click interested category",
  "keyword": "Then "
});
formatter.match({
  "location": "produkdiminati.user_click_interested_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to my interested product page",
  "keyword": "Then "
});
formatter.match({
  "location": "produkdiminati.user_is_navigated_to_my_interested_product_page()"
});
formatter.result({
  "status": "passed"
});
});