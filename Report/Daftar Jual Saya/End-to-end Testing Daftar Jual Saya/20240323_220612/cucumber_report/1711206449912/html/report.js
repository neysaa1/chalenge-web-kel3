$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/produkterjual.feature");
formatter.feature({
  "name": "produk terjual",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "the user want to see my sold product list in my product page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user click my product menu button",
  "keyword": "Given "
});
formatter.match({
  "location": "produkterjual.theuserclickmyproductmenubutton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click sold product category button",
  "keyword": "When "
});
formatter.match({
  "location": "produkterjual.theuserclicksoldproductcategotybutton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user is navigated to my sold product list page",
  "keyword": "Then "
});
formatter.match({
  "location": "produkterjual.theuserinavigatedtomyosldproductlistpage()"
});
formatter.result({
  "status": "passed"
});
});