$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/wrkng/Desktop/Platinum challange/chalenge-web-kel3/Include/features/dlogout.feature");
formatter.feature({
  "name": "dlogout",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "the user want to logout",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "the user press menu icon button",
  "keyword": "Given "
});
formatter.match({
  "location": "dlogout.theuserpressmenuiconbutton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user press logout button",
  "keyword": "When "
});
formatter.match({
  "location": "dlogout.theuserpresslogoutbutton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user navigated to homepage to verify user has been logout",
  "keyword": "Then "
});
formatter.match({
  "location": "dlogout.theusernavigatedtohomepagetobevifylogout()"
});
formatter.result({
  "status": "passed"
});
});