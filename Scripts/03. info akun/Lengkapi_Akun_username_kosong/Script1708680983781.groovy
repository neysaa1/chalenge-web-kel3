import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.annotation.Keyword as Keyword
import java.awt.Robot as Robot
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import java.awt.event.KeyEvent as KeyEvent

not_run: WebUI.openBrowser('https://secondhand.binaracademy.org/')

WebUI.callTestCase(findTestCase('02. login/login_valid_data'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Info Akun/Page_SecondHand/01. text_profile'))

WebUI.click(findTestObject('Info Akun/Page_SecondHand/02. logo_profile'))

not_run: WebUI.click(findTestObject('Info Akun/Page_SecondHand/03. button_Photo_Profile'))

WebUI.setText(findTestObject('Object Repository/Info Akun/Page_SecondHand/05. text_username'), '')

WebUI.selectOptionByValue(findTestObject('Object Repository/Info Akun/Page_SecondHand/06. text_kota'), '4', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Info Akun/Page_SecondHand/06. text_kota'), '4', true)

WebUI.setText(findTestObject('Object Repository/Info Akun/Page_SecondHand/07. text_Alamat'), 'Jalan Kelompok 3')

WebUI.setText(findTestObject('Object Repository/Info Akun/Page_SecondHand/08. text_NoHP'), '0813 6161 2202')

WebUI.scrollToElement(findTestObject('Info Akun/Page_SecondHand/09. button_simpan'), 0)

WebUI.click(findTestObject('Info Akun/Page_SecondHand/09. button_simpan'))

not_run: WebUI.verifyElementVisible(findTestObject('login/Page_SecondHand/05. verify logo'))

