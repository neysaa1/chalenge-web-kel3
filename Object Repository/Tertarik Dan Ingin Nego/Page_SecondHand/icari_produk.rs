<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icari_produk</name>
   <tag></tag>
   <elementGuidId>9bc84659-f222-47dc-93fb-c64cf3156836</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.px-0.border-0.shadow.h-100.pb-4.rounded-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d6554d44-c5b0-49b9-935c-38475af55c70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>c5fbb5de-b96e-4d5d-b066-2435d690a78e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      Flammenwarfen
      Hobi
      Rp 100
    
  </value>
      <webElementGuid>f326a03d-0245-4225-97de-d6f090099f99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>a1da0e54-d275-45e9-bb9c-c700f0cd7875</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div/a/div</value>
      <webElementGuid>a7446d00-5911-437f-adde-2bcece8c4e50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div</value>
      <webElementGuid>2ba30d7e-ea64-429d-9d39-96e2011af536</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      Flammenwarfen
      Hobi
      Rp 100
    
  ' or . = '
      

    
      Flammenwarfen
      Hobi
      Rp 100
    
  ')]</value>
      <webElementGuid>6700441a-5e87-41d2-b46e-babb151396ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
