<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Diminati</name>
   <tag></tag>
   <elementGuidId>83ffb4a6-7e59-44c0-b682-73877e06a5d6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.d-flex.py-3.border-bottom.border-1.active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua Produk'])[1]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>cbe2b392-5e1d-4df5-bb67-969005231c3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link d-flex py-3 border-bottom border-1 active</value>
      <webElementGuid>5e1ea1c3-bb53-4b83-b13a-bf482bfb52ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/products?product%5Bliked%5D=true&amp;product%5Bsold%5D=false</value>
      <webElementGuid>03c5e4d3-d953-4604-a7e8-79f24aa9a88f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
              Diminati
              
</value>
      <webElementGuid>1a0f24b6-da9b-46d5-9e6a-080d3b09abfe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-3&quot;]/div[@class=&quot;card border-0 rounded-4 shadow p-4&quot;]/ul[@class=&quot;nav flex-column&quot;]/li[@class=&quot;nav-item&quot;]/a[@class=&quot;nav-link d-flex py-3 border-bottom border-1 active&quot;]</value>
      <webElementGuid>1db9e182-d324-452a-b016-02a16b98845b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua Produk'])[1]/following::a[1]</value>
      <webElementGuid>483d7085-d4ca-4aed-b43c-598e8ce71ae9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::a[2]</value>
      <webElementGuid>676a8cf5-0391-42dd-a57a-fd30edbefe40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/preceding::a[1]</value>
      <webElementGuid>6a865313-0ac5-4780-b28a-74df494eb4d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products?product%5Bliked%5D=true&amp;product%5Bsold%5D=false')]</value>
      <webElementGuid>bdc35060-d4df-45f0-9ce2-527927287db9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>44e7785d-b66a-45ec-8de4-6f85fe849f14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products?product%5Bliked%5D=true&amp;product%5Bsold%5D=false' and (text() = '
              
              Diminati
              
' or . = '
              
              Diminati
              
')]</value>
      <webElementGuid>123ad051-1439-4c92-b213-370e8f628b76</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
