<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_daftar_jual_saya</name>
   <tag></tag>
   <elementGuidId>2d96b6a4-e076-4775-bf5b-363363e9a7cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.fw-bold.my-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung'])[1]/following::h3[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar Jual Saya</value>
      <webElementGuid>0e2a9db7-2ab6-44c9-9928-eafcd5b8e916</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung'])[1]/following::h3[1]</value>
      <webElementGuid>7cb9ac93-04d1-419d-9e66-81d32cf4e23f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Leo Juga'])[1]/preceding::h3[1]</value>
      <webElementGuid>9a88fc3b-714a-4f76-8c42-2cb5137bac2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bandung'])[2]/preceding::h3[1]</value>
      <webElementGuid>c2192a8b-ea8e-4d73-9b99-7e8279116eea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Jual Saya']/parent::*</value>
      <webElementGuid>32e884be-da00-4bd1-94bf-e4f7aa723088</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>551daa18-d3b1-4347-a67a-6a50a09ba13a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Daftar Jual Saya' or . = 'Daftar Jual Saya')]</value>
      <webElementGuid>c34ef691-25d8-4c99-9bc9-b79fe1ccffb7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
