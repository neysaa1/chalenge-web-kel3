<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_diminati</name>
   <tag></tag>
   <elementGuidId>dffa963b-dd3d-44a1-8a7f-797e2f6ee2b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row.g-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a13c8e43-caee-4e77-84c9-8f1cb2527de3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row g-4</value>
      <webElementGuid>4b05eded-f7c6-427e-84ba-6578f0ab5a58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

            
              
  
      

    
      sarung tangan kiper
      Hobi
      Rp 250.000
    
  

            

          
      </value>
      <webElementGuid>516489b9-0bb9-4335-9b50-29eb111c04ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]</value>
      <webElementGuid>2b34f3ba-bd74-4822-8dd7-349f213ba69a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::div[2]</value>
      <webElementGuid>3dedbb86-5f6d-4049-89e9-34dd8bd27dd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/following::div[2]</value>
      <webElementGuid>ae9daacd-12b7-4507-97e4-255e798fa490</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div</value>
      <webElementGuid>c89ce0f9-33c4-4998-83c8-2df3ffd8fcc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

            
              
  
      

    
      sarung tangan kiper
      Hobi
      Rp 250.000
    
  

            

          
      ' or . = '

            
              
  
      

    
      sarung tangan kiper
      Hobi
      Rp 250.000
    
  

            

          
      ')]</value>
      <webElementGuid>f7f3bbca-3339-4db0-b24b-f7b9688a63f8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
