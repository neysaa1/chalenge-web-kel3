<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_penawaran_produk</name>
   <tag></tag>
   <elementGuidId>ca9c232f-4a99-48d6-a737-dbf9d9b8845b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.notification.my-4.px-2.position-relative</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>a6f2eee0-ea7d-415a-955f-73f625b48e88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification my-4 px-2 position-relative</value>
      <webElementGuid>f87dd045-e1c6-4cec-aa06-09271eb9107a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-id</name>
      <type>Main</type>
      <value>183502</value>
      <webElementGuid>ce57dee6-9c14-4e6d-b592-471f49cca2e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-read</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d68d9f8d-b43e-4d97-866e-2174f285bb69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/users/83221/offers</value>
      <webElementGuid>53781c87-45d8-4a88-afe1-a888a31681c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
              
                Penawaran produk
                sarung tangan kiperRp 250.000Ditawar Rp 150.000
              
              08 Mar, 21:28

                
                  New alerts
                
</value>
      <webElementGuid>f407144c-0bd6-4bc0-b0c2-789251e46b2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/ul[@class=&quot;dropdown-menu notification-dropdown-menu px-4 show&quot;]/li[1]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]</value>
      <webElementGuid>5f9d8a7f-7fe4-4663-87b9-5aedb68a6a14</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/ul/li/a</value>
      <webElementGuid>b4e207fe-2c55-44c6-957c-1a6dc166aada</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[2]/following::a[1]</value>
      <webElementGuid>41b30d8b-acf3-440c-b39b-7a882794b61e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[1]/following::a[2]</value>
      <webElementGuid>5d0af16e-f1a4-406d-a117-01be2e10f28c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/users/83221/offers')]</value>
      <webElementGuid>883870dc-6246-4f55-9a9c-c6527610d0eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul/li/a</value>
      <webElementGuid>2c35c25f-25fd-438c-b95e-8815f783ec8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/users/83221/offers' and (text() = '
                
              
                Penawaran produk
                sarung tangan kiperRp 250.000Ditawar Rp 150.000
              
              08 Mar, 21:28

                
                  New alerts
                
' or . = '
                
              
                Penawaran produk
                sarung tangan kiperRp 250.000Ditawar Rp 150.000
              
              08 Mar, 21:28

                
                  New alerts
                
')]</value>
      <webElementGuid>e6cc7d2a-dcb0-4eb0-a6d5-18d8d0e8ca2c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
