<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_product_offers</name>
   <tag></tag>
   <elementGuidId>e44d553f-b0c5-4578-adf7-6f9c968bf2be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Info Penawar'])[1]/following::section[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>section.container-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>a3b1fa2b-58a3-4741-9005-42e1a32ab9d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container-sm</value>
      <webElementGuid>feaa50e5-74a1-480e-8e48-6751c0d5858b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  
    
      

      
        Putri Testing 2
        Jakarta
      
    
  

  Daftar Produkmu yang Ditawar

  
      
        
        
          
            
              Penawaran produk diterima
            
            
              08 Mar, 21:28
            
          
          
            
              sarung tangan kiper
            
            
              Rp 250.000
            
            
              Ditawar Rp 150.000
            
          
          
              Status
              
                Hubungi di 
          
        
      

      
        
          
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
        
      

        
  
</value>
      <webElementGuid>47965845-4ed2-4e7f-b135-c53020e5ef32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]</value>
      <webElementGuid>2b14d6d5-72fc-4026-97d4-771b2410b31c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Info Penawar'])[1]/following::section[2]</value>
      <webElementGuid>e630aede-671c-48a5-b9dc-768068c3b6d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/section</value>
      <webElementGuid>ee41d0bc-445e-4750-a18e-4d4f51bc255f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[(text() = '
  
    
      

      
        Putri Testing 2
        Jakarta
      
    
  

  Daftar Produkmu yang Ditawar

  
      
        
        
          
            
              Penawaran produk diterima
            
            
              08 Mar, 21:28
            
          
          
            
              sarung tangan kiper
            
            
              Rp 250.000
            
            
              Ditawar Rp 150.000
            
          
          
              Status
              
                Hubungi di 
          
        
      

      
        
          
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
        
      

        
  
' or . = '
  
    
      

      
        Putri Testing 2
        Jakarta
      
    
  

  Daftar Produkmu yang Ditawar

  
      
        
        
          
            
              Penawaran produk diterima
            
            
              08 Mar, 21:28
            
          
          
            
              sarung tangan kiper
            
            
              Rp 250.000
            
            
              Ditawar Rp 150.000
            
          
          
              Status
              
                Hubungi di 
          
        
      

      
        
          
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
        
      

        
  
')]</value>
      <webElementGuid>a471e128-062b-42e0-9e82-c7a74eda7a68</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
