<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email_useremail</name>
   <tag></tag>
   <elementGuidId>63fca35a-5840-456d-a7fe-e3167eacaf7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#user_email</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='user_email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ec771155-e713-452f-8937-30004720e5b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>ba594b46-e133-44be-b91b-7f6244947b2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autofocus</name>
      <type>Main</type>
      <value>autofocus</value>
      <webElementGuid>1b9a1993-4dbd-4b09-ad34-fa3116c2944d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>a960ab62-d9ef-4939-bd42-1d4816604b93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>0e4c30f1-dece-4a0a-8fe8-d2e1e8b5fd4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: johndee@gmail.com</value>
      <webElementGuid>75ad4f86-3e74-488e-a479-93eac2f29efb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>51a49319-9c40-4792-9b80-b3a8b3a7fe15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user[email]</value>
      <webElementGuid>97c634fb-2083-4605-9454-c5819811af23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_email</value>
      <webElementGuid>2e0acb53-388d-452a-aa82-0ebc4360ecde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user_email&quot;)</value>
      <webElementGuid>6584cfdc-52d5-40bd-85ee-25017d914b3a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='user_email']</value>
      <webElementGuid>3fa0f962-30fd-4364-b60e-bf06d4b979e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div/input</value>
      <webElementGuid>2ba047bb-65d3-4cc0-92c7-0d6c8d581e53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>6967ae10-a872-459e-b766-4196544caf6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Contoh: johndee@gmail.com' and @type = 'email' and @name = 'user[email]' and @id = 'user_email']</value>
      <webElementGuid>26649742-fe15-461e-8211-6cf7911fb3ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
