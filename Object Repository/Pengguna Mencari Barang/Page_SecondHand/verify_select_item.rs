<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify_select_item</name>
   <tag></tag>
   <elementGuidId>50afdf21-b736-4eab-bc66-ae98573e535b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.px-0.border-0.shadow.h-100.pb-4.rounded-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='products']/div/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6228285a-b412-4355-bfd0-7525957e04a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>7da954bd-3262-4444-86ef-be5d5d53bf5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      MacBook
      Elektronik
      Rp 20.000.000
    
  </value>
      <webElementGuid>8feac435-ce1d-4e97-b4f0-f1e5a0517a15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;products&quot;)/div[@class=&quot;col-12 col-md-4 col-lg-3&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>6e835e21-2488-4c7b-8d35-e8c318f61962</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='products']/div/a/div</value>
      <webElementGuid>d0504359-d58d-49b5-96ca-34744a09f313</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div</value>
      <webElementGuid>ab3abd1c-c009-4941-a0c3-20ec68ade223</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      MacBook
      Elektronik
      Rp 20.000.000
    
  ' or . = '
      

    
      MacBook
      Elektronik
      Rp 20.000.000
    
  ')]</value>
      <webElementGuid>e9370de6-45cf-4ba8-8269-4bfe9b22be6a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
