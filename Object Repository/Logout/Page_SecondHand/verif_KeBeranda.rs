<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verif_KeBeranda</name>
   <tag></tag>
   <elementGuidId>e900c134-8b41-4a57-b20b-d14b91f79165</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.fw-bold.mb-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Signed out successfully.'])[1]/following::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>f7cefa34-e3ba-40b9-91a2-1348747b7f39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold mb-4</value>
      <webElementGuid>36dfbd78-e05a-4521-b640-a544a0a21a97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bulan RamadhanBanyak diskon!</value>
      <webElementGuid>140142f4-87b4-4aea-8f7c-88947eac34f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;mt-lg-5 mb-5 w-100 d-flex gap-lg-5&quot;]/div[@class=&quot;promo center flex-grow-1 bg-warning rounded-lg-4 rounded-0&quot;]/div[@class=&quot;row p-5&quot;]/div[@class=&quot;col&quot;]/h1[@class=&quot;fw-bold mb-4&quot;]</value>
      <webElementGuid>d75df2b1-16ad-44b2-a19e-94d993d1701f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Signed out successfully.'])[1]/following::h1[1]</value>
      <webElementGuid>7c45bf12-b1b2-4f5b-8b3a-d2852d988a63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/preceding::h1[1]</value>
      <webElementGuid>e9d2026e-1d72-4f1d-9209-907ff1c39fe1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telusuri Kategori'])[1]/preceding::h1[1]</value>
      <webElementGuid>343d7493-6721-45c9-ade5-5242114171df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bulan Ramadhan']/parent::*</value>
      <webElementGuid>0570dc6f-83e2-4013-8aec-689087b43fc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>616980d6-9057-4af3-a6a7-1e7d45dbdeee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Bulan RamadhanBanyak diskon!' or . = 'Bulan RamadhanBanyak diskon!')]</value>
      <webElementGuid>411567ef-4c04-42c2-be52-ebec24f74884</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
