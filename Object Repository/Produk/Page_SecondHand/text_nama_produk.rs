<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_nama_produk</name>
   <tag></tag>
   <elementGuidId>87876024-10c2-41b6-a18b-64bcc718ccb1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='product_name']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#product_name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_name</value>
      <webElementGuid>f8ae5dff-d041-43fd-b03a-7b09e1ad88a9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='product_name']</value>
      <webElementGuid>7068009a-ba73-4b12-95df-c6d57459b25f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/form/div/input</value>
      <webElementGuid>6e16e8db-a682-4e2b-8d17-e9011dad46cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Nama Produk' and @type = 'text' and @name = 'product[name]' and @id = 'product_name']</value>
      <webElementGuid>fbbde561-9919-4509-a9f5-be03484214e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
