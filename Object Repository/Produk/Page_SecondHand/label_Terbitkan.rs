<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Terbitkan</name>
   <tag></tag>
   <elementGuidId>e3287645-30d4-4cb6-8f14-b3f36c9d9a57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/following::label[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-primary w-50 rounded-4 p-3']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>label.btn.btn-primary.w-50.rounded-4.p-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary w-50 rounded-4 p-3</value>
      <webElementGuid>6ae21e23-2ac9-4b7a-9002-3ce451d5e10f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Preview'])[1]/following::label[1]</value>
      <webElementGuid>c8aed21c-c964-4b4c-abb2-aeb7158d3086</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::label[2]</value>
      <webElementGuid>1822f987-0e42-41f3-ac6d-ada0fc1899e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Terbitkan']/parent::*</value>
      <webElementGuid>f368d0a6-d935-4cc1-8507-616424e6db21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[2]</value>
      <webElementGuid>ceebe9f4-18f4-4db3-a2ab-40ebb6740b3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Terbitkan' or . = 'Terbitkan')]</value>
      <webElementGuid>0463ac28-76b1-4a06-80fb-962fe873122a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
