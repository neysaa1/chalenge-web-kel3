<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_delete</name>
   <tag></tag>
   <elementGuidId>a1b73d16-f4b5-40b3-aa47-d16c35a32e54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-lg btn-danger fs-6 rounded-4 w-100 py-3 fw-bold mt-3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-lg btn-danger fs-6 rounded-4 w-100 py-3 fw-bold mt-3</value>
      <webElementGuid>d46402f1-14f9-4c8f-997d-666f91219de8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
