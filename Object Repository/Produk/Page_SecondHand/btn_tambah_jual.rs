<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_tambah_jual</name>
   <tag></tag>
   <elementGuidId>5980cbe4-fb18-4c21-a281-b4643d0f3ae1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Next →'])[1]/following::a[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-primary.btn-lg.rounded-4.d-inline-flex.align-items-center.justify-content-space-between.px-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg rounded-4 d-inline-flex align-items-center justify-content-space-between px-5</value>
      <webElementGuid>4395f2e9-62be-4cde-bbac-0282c6b5cb90</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next →'])[1]/following::a[1]</value>
      <webElementGuid>8f694896-742d-4aff-979c-cf42188c442e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='← Previous'])[1]/following::a[2]</value>
      <webElementGuid>91fef392-3df6-4b09-844b-12e66c6c8f8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/products/new')]</value>
      <webElementGuid>2cf19a85-e141-42d5-bf08-df47ee199dd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/a</value>
      <webElementGuid>db19ab9f-615d-48d4-baa3-bd3f0bfe9641</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/products/new' and (text() = '
    
    Jual
' or . = '
    
    Jual
')]</value>
      <webElementGuid>c7910b56-1616-4340-abd1-966661c74fa1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
