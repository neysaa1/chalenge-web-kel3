<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verify product page</name>
   <tag></tag>
   <elementGuidId>76448b55-330e-4d31-a096-95ac7bd2a738</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Produk'])[1]/following::div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
      

    
      test 1213 nama
      Kendaraan
      Rp 10.000
    
  ' or . = '
      

    
      test 1213 nama
      Kendaraan
      Rp 10.000
    
  ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card.px-0.border-0.shadow.h-100.pb-4.rounded-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f21106ba-56df-459c-968c-700dc8e6aa4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card px-0 border-0 shadow h-100 pb-4 rounded-4</value>
      <webElementGuid>c5b07871-1667-4c61-b6b2-56a515c1e43d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
      

    
      test 1213 nama
      Kendaraan
      Rp 10.000
    
  </value>
      <webElementGuid>a458e5fa-9e34-40e5-9f4c-e669e828c9ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;product-card&quot;]/div[@class=&quot;card px-0 border-0 shadow h-100 pb-4 rounded-4&quot;]</value>
      <webElementGuid>0998ab5d-9b97-4fb4-af08-b2935ad70745</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Produk'])[1]/following::div[2]</value>
      <webElementGuid>707a11bb-ae2a-4fef-9986-37edeef449e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/following::div[6]</value>
      <webElementGuid>a0bb03ae-07e0-4558-b28c-1e8aa51724f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a/div</value>
      <webElementGuid>66a509b7-6fec-4ba2-95b7-8210a42dc76c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
      

    
      test 1213 nama
      Kendaraan
      Rp 10.000
    
  ' or . = '
      

    
      test 1213 nama
      Kendaraan
      Rp 10.000
    
  ')]</value>
      <webElementGuid>e74f9d4d-8639-48d7-979f-ed13a348f69e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
