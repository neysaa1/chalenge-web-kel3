<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>verif_to_notif_page</name>
   <tag></tag>
   <elementGuidId>3d770305-3756-4664-9432-2b2383aaa92d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::ul[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>ac8316c8-1c43-41a0-b466-07f128e2b310</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>p-5 notification-list</value>
      <webElementGuid>b5f0277b-f7de-4a56-ac7e-e7b83bf7a2f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
          
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 700.000
            
            28 Sep, 23:16

        

          
        
          
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 100.000
            
            20 Sep, 20:50

        

          
        
          
              
            
              Berhasil di terbitkan
              Baju delimaRp 1.200.000
            
            20 Sep, 20:48

        

          
        
          
              
            
              Berhasil di terbitkan
              Baju delimaRp 180.000
            
            20 Sep, 20:34

        

  </value>
      <webElementGuid>9439d55d-562f-43fc-8e48-43fb070a4596</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]</value>
      <webElementGuid>89feccef-9d98-4cf5-93c5-b627501c55fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::ul[1]</value>
      <webElementGuid>9e583d5a-55ad-4b72-8f1c-676bed707313</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul</value>
      <webElementGuid>10da60f5-b6d7-4c78-a372-6bb788606ed3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = '
        
          
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 700.000
            
            28 Sep, 23:16

        

          
        
          
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 100.000
            
            20 Sep, 20:50

        

          
        
          
              
            
              Berhasil di terbitkan
              Baju delimaRp 1.200.000
            
            20 Sep, 20:48

        

          
        
          
              
            
              Berhasil di terbitkan
              Baju delimaRp 180.000
            
            20 Sep, 20:34

        

  ' or . = '
        
          
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 700.000
            
            28 Sep, 23:16

        

          
        
          
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 100.000
            
            20 Sep, 20:50

        

          
        
          
              
            
              Berhasil di terbitkan
              Baju delimaRp 1.200.000
            
            20 Sep, 20:48

        

          
        
          
              
            
              Berhasil di terbitkan
              Baju delimaRp 180.000
            
            20 Sep, 20:34

        

  ')]</value>
      <webElementGuid>dbf703ea-457d-4820-b7ae-0a168d74a02c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
