<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_penawaran_produk</name>
   <tag></tag>
   <elementGuidId>b6315a6d-04f0-4d10-a103-7fd5dfbf8768</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li > a.notification.my-4.px-2.position-relative</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e95cede4-41df-4c03-b8f3-59ba21fd2b01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification my-4 px-2 position-relative</value>
      <webElementGuid>13601c50-f2e4-4e30-be95-e78a03280b7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-id</name>
      <type>Main</type>
      <value>68805</value>
      <webElementGuid>248df567-a7c6-4998-b758-19a513fcdb38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-notification-read</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>f8eb5312-e963-49d7-af3e-8b61d6296326</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/users/75832/offers</value>
      <webElementGuid>71ff9ef2-92b1-434c-880d-4eb137e64ca8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 700.000
            
            28 Sep, 23:16

</value>
      <webElementGuid>055faf09-bd4b-41eb-a458-08c445da433c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[1]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]</value>
      <webElementGuid>08b5a175-94d2-46ab-ae2e-011a3fa20046</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jakarta'])[1]/following::a[1]</value>
      <webElementGuid>89f43ea1-35a4-4658-a41f-c7b714ec749c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/users/75832/offers')])[2]</value>
      <webElementGuid>099f6267-2652-4ce7-9477-8bbd654eed88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li/a</value>
      <webElementGuid>c6e62e8d-5365-436a-9e20-20b0438c2456</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/users/75832/offers' and (text() = '
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 700.000
            
            28 Sep, 23:16

' or . = '
              
            
              Penawaran produk
              Baju delimaRp 1.200.000Ditawar Rp 700.000
            
            28 Sep, 23:16

')]</value>
      <webElementGuid>fdab344c-16a1-4a55-8a29-49af6a95068f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
